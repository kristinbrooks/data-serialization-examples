#!/usr/bin/env ruby

require 'json'
require 'psych'

class JsonToYaml
  def run
    # read the json file into a string
    json_string = File.read("activities.json")
    # deserialize the json string into a ruby hash
    activities_hash = JSON.parse(json_string)
    p activities_hash.class
    activities_yaml = Psych.dump(activities_hash)
    p activities_yaml.class
    puts activities_yaml
  end
end

JsonToYaml.new.run
