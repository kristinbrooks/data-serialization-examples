#!/usr/bin/env ruby

require "psych"

class YamlToJson
  def run
    # deserializes the yaml file and store it in a ruby hash
    activities_hash = Psych.load_file("activities.yaml")
    p activities_hash.class
    # serializes the hash to json
    activities_json = Psych.to_json(activities_hash)
    p activities_json.class
    puts activities_json
  end
end

YamlToJson.new.run
